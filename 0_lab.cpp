#include <iostream>
#include <map>
#include <fstream>

using namespace std;

class MapInvertor {
 public:
    static void inverseMap( map <string, int> & inputMap, multimap <int, string> & inverseMap ) {
        auto it = inputMap.begin();
        for ( ; it != inputMap.end(); it++ ) {
            inverseMap.insert( make_pair( it->second, it->first ) );
        }
    }
};


class Parser {
 public:
    static void parseAndCount( ifstream& fin, map <string, int> & counterMap, int & numberOfWords ) {
        string buffer, word;
        while ( getline( fin, buffer ) ) {
            for ( int i = 0; i <= buffer.size(); i++ ) {
                if ( isNumberOrLetter( buffer[i] ) ) { // Собираем слово до первого разделителя
                    word += buffer[i];
                }
                else if ( word != "" ) { // Отсекаем случаи, когда разделители идут подряд
                    Parser::increaseWordMapCounter( counterMap, word );
                    numberOfWords++;
                    word = "";
                }
            }
        }
    }
    static int isNumberOrLetter( char symbol ) {
        if ( ( ( '0' <= symbol ) && ( symbol <= '9' ) )
          || ( ( 'a' <= symbol ) && ( symbol <= 'z' ) )
          || ( ( 'A' <= symbol ) && ( symbol <= 'Z' ) )
        ) return 1;
        else return 0;
    }

 private:
    static void increaseWordMapCounter( map <string, int> & counterMap, string word ) {
        auto it = counterMap.begin();
        counterMap.find( word );
        if ( it == counterMap.end() ) {
            counterMap[word] = 0; // Если слово ещё не встречалось
        }
        counterMap[word]++;
    }
};

class Writer {
 public:
    static void writeMapInCSV( ofstream& fout, multimap <int, string> & counterMap, int numberOfWords ) {
        float frequency = 0;
        auto it = counterMap.end();
        it--;
        for (; it != counterMap.begin(); it-- ) { // Идём в обратном направлении, чтобы выводить частоту по убыванию
            frequency = (float)it->first / numberOfWords;
            frequency *= 100; // Перевод частоты в %
            fout << it->second << "," << it->first << "," << frequency << "%" << endl;
        }
        fout << it->second << "," << it->first << "," << frequency << "%" << endl;
    }
};

int main( int argc, char * argv[] ) {

    map <string, int> counterMap;
    multimap <int, string> inverseCounterMap;
    int numberOfWords = 0;
    ifstream fin( argv[1] );
    ofstream fout( argv[2] );

    Parser::parseAndCount( fin, counterMap, numberOfWords );
    MapInvertor::inverseMap( counterMap, inverseCounterMap );
    Writer::writeMapInCSV( fout, inverseCounterMap, numberOfWords );
    fout.close();
    return 0;
}
